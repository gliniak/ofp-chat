const port = 8080;
const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const path = require('path');

app.use('/static', express.static(path.join(__dirname, 'static')));
app.get('/', function(req, res) {
  res.sendFile(__dirname + '/chat.html');
});

const messagesSize = 100;
const messages = [];

io.on('connection', function(socket) {
  console.log('a user connected');
  socket.emit('history', messages);

  let userName;
  socket.on('userName', (messageText) => {
      userName = messageText;
  });

  socket.on('messageText', (messageText) => {
      console.log(userName, 'sent:', messageText);
      message = {
          user: userName,
          date: new Date().toISOString(),
          text: messageText
      };
      messages.push(message);
      messages.slice(-messagesSize);

      io.emit('message', message);
  });

  socket.on('disconnect', () => { console.log('user disconnected'); });
});

http.listen(port, () => { console.log(`listening on:${port}`); });
